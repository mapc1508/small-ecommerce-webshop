var app = angular.module('ecommerceApp', ['ngRoute', 'rzModule']);

app.run(['$rootScope', '$location', '$routeParams', function ($rootScope, $location, $routeParams) {
    $rootScope.$on("$routeChangeSuccess", function (e, currentRoute, previousRoute) {
        //Change page title, based on Route information
        $rootScope.title = currentRoute.$$route.title;
    });
}]);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'main.html',
            controller: 'mainController',
            title: 'Products list'
        })
        .when('/faq', {
            templateUrl: 'faq.html',
            controller: 'faqController',
            title: 'Faq'
        })
        .when('/about', {
            templateUrl: 'about.html',
            controller: 'aboutController',
            title: 'About'
        })
        .when('/contact', {
            templateUrl: 'contact.html',
            controller: 'contactController',
            title: 'Contact'
        })
        .when('/product', {
            templateUrl: 'product.html',
            controller: 'productController',
            title: 'Product Details'
        })
        .when('/shopping-cart', {
            templateUrl: 'shopping-cart.html',
            controller: 'cartController',
            title: 'Shopping Cart'
        })
        .when('/checkout-address', {
            templateUrl: 'checkout-address.html',
            //controller: 'faqController',
            title: 'Shopping Cart'
        })
        .when('/checkout-delivery', {
            templateUrl: 'checkout-delivery.html',
            controller: 'deliveryController',
            title: 'Shopping Cart'
        })
        .when('/checkout-payment', {
            templateUrl: 'checkout-payment.html',
            //controller: 'faqController',
            title: 'Shopping Cart'
        })
        .when('/checkout-review', {
            templateUrl: 'checkout-review.html',
            controller: 'orderReviewController',
            title: 'Shopping Cart'
        });

    $locationProvider.html5Mode(true);
});

app.service('productService', [function () {
    this.selectedProduct = {};
}]);

app.service('checkoutService', [function () {
    this.cart = [];
    this.standardDelivery = true;
    this.calculateTotal = function (items) {
        var total = 0;
        for (var i = 0; i < items.length; i++) {
            total += items[i].price;
        }
        return total;
    }
}]);

app.filter('unique', function () {
    return function (collection, keyname) {
        var output = [],
            keys = [];

        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
});

app.controller('mainController', ['$scope', 'productService', '$location', '$filter', function ($scope, productService, $location, $filter) {
    // List of product objects
    var products = [{
            name: "Asus Dual Nvidia Geforce GTX 1060 3GB DDR5",
            price: 249,
            new: true,
            description: "Just like in games, the exclusive TORX 2.0 Fan technology uses the power of teamwork to allow the TWIN FROZR VI to achieve new levels of cool. TORX 2.0 Fan design generates 22% more air pressure for supremely silent performance in the heat of battle. Double Ball Bearings give the unique TORX 2.0 Fans a strong and lasting core for years of smooth gaming. They also remain virtually silent while spinning under load, keeping your graphics card cool during intense and lengthy gaming sessions.",
            details: [
                "Chipset: NVIDIA GeForce GTX 1060",
                "Video Memory: 6GB GDDR5",
                "Max. Resolution: 7680 x 4320, support 4x Display monitors",
                "Input: 1x 8Pin PCI-E power connector, output: DVI-D Dual Link, HDMI, 3x DisplayPort's",
                "400W system power supply requirement; 120W power consumption. Please Note: Kindly refer the User Manual and User Guide before use."
            ],
            inStock: 4,
            images: ["1/1_1.jpg", "1/1_2.jpg", "1/1_3.jpg"],
            brand: "Asus",
            category: "Pc Hardware",
            subCategory: "GPU"
        },
        {
            name: "Asus Zenbook Flip UX360CA",
            price: 749,
            new: false,
            description: "ASUS ZenBook Flip is lighter, slimmer and faster than other notebooks in its class to give you the unmatched power and all-day portability you need to conquer your day. 360-degree rotation to fit and allow you to instantly switch between 4 modes-laptop, tablet, tent and stand mode.",
            details: [
                "13.3-Inch wide-view Full-HD Touch Display with Windows 10 Pre-installed",
                "Latest 7th generation Intel i5-7Y54 1.2 GHz Processor (Turbo to 3.2 GHz)",
                "Extensive connectivity with HDMI/D-SUB/USB",
                "Fast 512GB SSD with 8GB DDR3 RAM",
                "Sleek and light weight 3.0 lbs aluminum body for comfortable portability"
            ],
            inStock: 2,
            images: ["2/2_1.jpg", "2/2_2.jpg", "2/2_3.jpg"],
            brand: "Asus",
            category: "Laptops"
        },
        {
            name: "Lenovo Miix 310 10.1",
            price: 335,
            discount: 0.15,
            new: false,
            description: "A 10 inch tablet and PC in one, the affordable Miix 310 is eminently mobile, so you won’t feel confined to a physical keyboard or budget. The included detachable keyboard makes it easy to convert from PC to tablet mode, and the battery can last for up to 10 hours of local video playback on the gorgeous display. ",
            details: [
                "Screen Size: 10.1 inches",
                "Max Screen Resolution: 1280x800 pixels",
                "Processor:	1.44 GHz Atom Z3735F",
                "RAM: 2 GB DDR3",
                "Hard Drive: 32GB"
            ],
            inStock: 1,
            images: ["3/3_1.jpg", "3/3_2.jpg", "3/3_3.jpg"],
            brand: "Lenovo",
            category: "Tablets"
        },
        {
            name: "SkyTech ArchAngel GTX 1050 Ti Gaming PC",
            price: 749,
            new: false,
            description: "Now you can turn your PC into a powerful gaming rig! With the newest technology from Nvidia GTX 1050 Ti, SkyTech PC provides you the newest and improved version of the Pascal architecture at a budget. Redeem your confidence while you raise your gaming experience and praise our SkyTech PC. The Skytech ArchAngel 1050 Ti offers a fast and powerful performance that all gamers will love. ",
            details: [
                "AMD FX-6300 3.50GHz / 4.1 Turbo 6-Core",
                "1 TB 7200RPM Hard Drive",
                "AMD 970 Chipset Gaming Motherboard",
                "8GB Gaming Memory DDR3 1866 MHz with Heat Spreader",
                "GTX 1050 Ti 4GB GDDR5 Graphics Card",
                "1 x DL-DVI, 1 x Display Port 1.4, 1 x HDMI 2.0b | 9 x USB"
            ],
            inStock: 3,
            images: ["4/4_1.jpg", "4/4_2.jpg", "4/4_3.jpg"],
            brand: "SkyTech",
            category: "Desktop PC",
            subCategory: "Gaming PC"
        },
        {
            name: "Acer Aspire Desktop, 7th Gen Intel Core i5-7400",
            price: 449,
            new: false,
            description: "Free your creativity! Even demanding tasks like video editing and compiling photo albums are easy with the Aspire TC Series desktop providing multitasking performance and easy expandability. The progressive black chassis with hairline finish offers front-panel media access so you can easily connect to your digital devices.The modern look of the Aspire TC Desktop with its sleek appearance adds style to your home decor and is both elegant and functional. With a 7th Gen Intel Core processor it delivers outstanding performance, quick response, and offers massive content storage capacity so you can play, create, do and enjoy more.",
            details: [
                "7th Generation Intel Core i5-7400 Processor 3.0GHz",
                "8GB DDR4 2400MHz Memory",
                "2TB 7200RPM SATA3 Hard Drive",
                "8X DVD-Writer Double-Layer Drive (DVD-RW)",
                "Intel HD Graphics 630",
                "High-Definition Audio with 5.1 Channel audio support"
            ],
            inStock: 4,
            images: ["5/5_1.jpg", "5/5_2.jpg"],
            brand: "Acer",
            category: "Desktop PC",
            subCategory: "Office PC"
        },
        {
            name: "Samsung Galaxy A3(2017) A320F DS 16GB",
            price: 295,
            discount: 0.1,
            new: true,
            description: "Crafted with minimalism in mind for maximum enjoyment its highly resistant rear 3D Glass and optimal 4.7” display fuse together flawlessly to provide a remarkably seamless design.Design that is a delight to hold. Sitting perfectly level with the rear casing the absence of any camera protrusion allows for uniformaly smooth and symmetrical design from all angles.Life looks great through the Galaxy A3 (2017). Fitted with a highly capable 13 MP rear camera you can capture the world around you as it was meant to be seen.",
            details: [
                "Screen: Super AMOLED capacitiv, 16M colors, Size: 4.7 inches",
                "OS: Android OS, v6.0.1 (Marshmallow)",
                "Camera: 13 MP",
                "CPU: Octa-core 1.6 GHz Cortex-A53",
                "Memory: 16GB ROM, 2GB RAM"
            ],
            inStock: 8,
            images: ["6/6_1.jpg", "6/6_2.jpg", "6/6_3.jpg"],
            brand: "Samsung",
            category: "Smartphones"
        }
    ];

    $scope.searchName;
    $scope.results = [];

    $scope.slider = {
        priceMax: 1000,
        priceMin: 0,
        options: {
            floor: 0,
            step: 1,
            ceiling: 1000,
            translate: function (value) {
                return '$' + value;
            }
        }
    };

    var getMaxPrice = function (products) {
        var maxPrice = 0;
        for (var i = 0; i < products.length; i++) {
            var element = products[i];
            if (element.price > maxPrice) maxPrice = element.price;
        }
        $scope.slider.priceMax = maxPrice;
        $scope.slider.options.ceiling = maxPrice;
    };

    getMaxPrice(products);

    var filterName = function () {
        $scope.results = $filter('filter')(products, $scope.searchName);
    };

    var filterPrice = function () {
        if ($scope.checkedBrands.length > 0) $scope.filterByBrand();

        $scope.results = $scope.results.filter(function (product) {
            return (product.price >= $scope.slider.priceMin && product.price <= $scope.slider.priceMax);
        });
    };

    $scope.orderProducts = function(){
        var orderType = $scope.orderType;
        console.log("orderType");

        if(orderType === '0') $scope.results = $filter('orderBy')($scope.results, 'name');
        else if(orderType === '1') $scope.results = $filter('orderBy')($scope.results, 'price');
        else if(orderType === '2') $scope.results = $filter('orderBy')($scope.results, '-price');
    };

    $scope.search = function () {
        filterName();
        filterPrice();
        categorize();
    };

    $scope.categoryList = [];

    var categorize = function () {
        $scope.categoryList = [];

        $scope.results.forEach(function (product) {
            var index = $scope.categoryList.map(function (e) {
                return e.name;
            }).indexOf(product.category);

            if (index < 0) {
                $scope.categoryList.push({
                    name: product.category,
                    count: 1,
                    subCategories: []
                });

                if (product.subCategory) {
                    var lastIndex = $scope.categoryList.length - 1;

                    $scope.categoryList[lastIndex].subCategories.push({
                        name: product.subCategory,
                        count: 1
                    });
                }
            } else {
                var current = $scope.categoryList[index];
                current.count++;

                var subcategoryIndex = current.subCategories.map(function (e) {
                    return e.name;
                }).indexOf(product.subCategory);

                if (subcategoryIndex < 0) {
                    current.subCategories.push({
                        name: product.subCategory,
                        count: 1
                    });
                } else {
                    current.subCategories[subcategoryIndex].count++;
                }
            }
        }, this);

        console.log($scope.categoryList);
    };

    $scope.checkedBrands = [];

    $scope.addToBrands = function (product) {
        if (product.checked) $scope.checkedBrands.push(product.brand);
        else {
            $scope.checkedBrands.splice($scope.checkedBrands.indexOf(product.brand), 1);
        }
    };

    $scope.filterByBrand = function () {
        $scope.results = $scope.results.filter(function (product) {
            return ($scope.checkedBrands.indexOf(product.brand) >= 0);
        });
        categorize();
    };

    $scope.clearBrands = function () {
        $scope.checkedBrands = [];
        $scope.results.forEach(function (product) {
            product.checked = false;
        }, this);
        $scope.search();
    }

    $scope.search();

    $scope.slider.options.onEnd = $scope.search;

    $scope.selectProduct = function (product) {
        productService.selectedProduct = product;
        $location.path("product");
    };
}]);

app.controller('productController', ['$scope', 'productService', 'checkoutService', function ($scope, productService, checkoutService) {
    $scope.product = productService.selectedProduct;
    $scope.addToCart = function (product) {
        checkoutService.cart.push(product);
        alert("The product has been added to your cart!");
        console.log(checkoutService.cart);
    };
}]);

app.controller('layoutController', ['$scope', 'checkoutService', function ($scope, checkoutService) {
    $scope.checkout = checkoutService;

    $scope.$watch('checkout.cart', function (newValue, oldValue) {
        if (newValue) $scope.itemsInTheBasket = newValue.length;
    }, true);
}]);

app.controller('cartController', ['$scope', 'checkoutService', function ($scope, checkoutService) {
    $scope.items = checkoutService.cart;

    $scope.$watch('items', function (newValue) {
        if (newValue) $scope.total = checkoutService.calculateTotal(newValue);
    }, true);

    $scope.removeFromCart = function (item) {
        var index = $scope.items.indexOf(item);
        $scope.items.splice(index, 1);
    }
}]);

app.controller('faqController', ['$scope', function ($scope) {}]);

app.controller('deliveryController', ['$scope', 'checkoutService', function ($scope, checkoutService) {
    $scope.standardDelivery = false;

    $scope.saveDeliveryMethod = function () {
        checkoutService.standardDelivery = $scope.standardDelivery;
        console.log(checkoutService.standardDelivery);
    };
}]);

app.controller('orderReviewController', ['$scope', 'checkoutService', '$location', function ($scope, checkoutService, $location) {
    $scope.items = checkoutService.cart;
    $scope.total = checkoutService.calculateTotal($scope.items);

    console.log(checkoutService.standardDelivery);

    if (checkoutService.standardDelivery === "true") $scope.shipping = 3;
    else $scope.shipping = 10;

    $scope.submitOrder = function () {
        alert("Your order has been submitted!");

        $scope.items.forEach(function (item) {
            item.inStock--;
        }, this);

        checkoutService.cart = [];
        $location.path("/");
    }
}]);